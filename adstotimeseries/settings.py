import os

ENRICHED_ADS_BASE_URL = os.environ.get('ENRICHED_ADS_BASE_URL', 'https://data.jobtechdev.se/annonser/historiska/berikade/kompletta/')
ENRICHED_AD_FILENAME_SUFFIX = os.environ.get('ENRICHED_AD_FILENAME_SUFFIX', '_beta1_jsonl.zip')
# Which year (4 digits) to download enriched ads for.
ENRICHED_AD_YEAR = os.environ.get('ENRICHED_AD_YEAR', '2016')
# 2016_beta1_jsonl.zip
S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME', 'ads-timeseries')
AWS_ACCESS_KEY = os.environ.get('AWS_ACCESS_KEY', 'minioadmin')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', 'minioadmin')
S3_URL = os.environ.get('S3_URL', 'http://localhost:9000')

# If the number of occurrences (e.g. for occupations and skills) should be counted based on the number_of_vacancies field or not.
# Default is USE_NUMBER_OF_VACANCIES = true
# If not: count every ad as one.
IGNORE_NUMBER_OF_VACANCIES = os.getenv('IGNORE_NUMBER_OF_VACANCIES', 'false').lower() == 'true'