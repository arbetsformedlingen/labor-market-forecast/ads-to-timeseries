from datetime import datetime
import logging
import os

import ndjson
import requests
import io
import zipfile
from adstotimeseries import settings
from adstotimeseries.minio_uploader import MinioUploader

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

def ads_to_timeseries():

    skills_timeseries = {}
    occupations_timeseries = {}

    minio_uploader = MinioUploader()

    ads_found = {}

    # TODO Change so that it only supports one year. Store the resulting timeseries file on minio for the year.
    # In the prediction step append the timeseries from the years files from minio into one single pandas dataframe.

    year = settings.ENRICHED_AD_YEAR
    url = settings.ENRICHED_ADS_BASE_URL + year + settings.ENRICHED_AD_FILENAME_SUFFIX
    log.info('Will download from URL: ' + url)

    ignore_number_of_vacancies = settings.IGNORE_NUMBER_OF_VACANCIES
    if ignore_number_of_vacancies:
        log.info('Ignore number of vacancies')
    else:
        log.info('use number of vacancies')

    currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'
    if not os.path.exists(currentdir + "ad_ids.txt"):
        f = open("ad_ids.txt", "w")
        f.close()

    file = open('ad_ids.txt', 'r')
    for row in file:
        ad_from_file = row.strip('\n')
        ads_found[ad_from_file] = ad_from_file
    file.close()

    response = requests.get(url)
    with zipfile.ZipFile(io.BytesIO(response.content)) as thezip:
        for zipinfo in thezip.infolist():
            with thezip.open(zipinfo) as ads_file:
                ads = ndjson.load(ads_file)
                for ad in ads:
                    id = None
                    if ad.get('original_id'):
                        ad_id = ad['original_id']
                        id = ad_id
                        if ad_id in ads_found:
                            log.warning(f'Ad with id {ad_id} already found')
                            continue
                    elif ad.get('id'):
                        generated_id = ad['id']
                        id = generated_id
                        if generated_id in ads_found:
                            log.warning(f'Ad with generated id {generated_id} already found')
                            continue
                    ads_found[id] = id
                    #info("ID: " +ad.get('id'));

                    if ad.get('publication_date'):
                        publication_date = ad['publication_date'] #'2016-11-03T00:00:00'
                        date_object = datetime.strptime(publication_date, '%Y-%m-%dT%H:%M:%S')
                        date_rounded = date_object.replace(hour=1, minute=00)
                        publication_date_millis = int(date_rounded.timestamp() * 1000)

                    number_of_vacancies = 1
                    if not ignore_number_of_vacancies:
                        if ad.get('number_of_vacancies'):
                            number_of_vacancies = int(ad['number_of_vacancies'])
                            if number_of_vacancies > 1:
                                log.info('More than one vacancy: ' + str(number_of_vacancies))

                    if ad.get('keywords'):
                        keywords = ad['keywords']
                        if keywords.get('enriched'):
                            enriched = keywords['enriched']
                            if enriched.get('occupation'):
                                occupations = enriched['occupation']
                                for occupation in occupations:
                                    if occupation not in occupations_timeseries:
#                                        occupations_timeseries[occupation] = {"occupation": occupation, "series": [{"date_timemillis": publication_date_millis, "occurences": number_of_vacancies}], "ads": [{"id": ad.get("id"), "original_id": ad.get("original_id")}]}
                                        occupations_timeseries[occupation] = {"occupation": occupation, "series": [{"date_timemillis": publication_date_millis, "occurrences": number_of_vacancies}]}
                                    else:
                                        occupation_dict = occupations_timeseries[occupation]
                                        # add ad to ads.
                                        # ads = occupation_dict['ads']
                                        # ads.append({"id": ad.get("id"), "original_id": ad.get("original_id"), "publication_date": publication_date_millis})
                                        # occupation_dict['ads'] = ads
                                        time_series = occupation_dict['series']
                                        # Check if on same date_timemillis, if so increment occurences.
                                        date_match = False
                                        for time_serie_dict in time_series:
                                            if time_serie_dict['date_timemillis'] == publication_date_millis:
                                                time_serie_dict["occurrences"] = time_serie_dict["occurrences"] + number_of_vacancies
                                                date_match = True
                                                break
                                        if not date_match:
                                            # else add as new element in series list.
                                            time_series.append({"date_timemillis": publication_date_millis, "occurrences": number_of_vacancies})
                                        occupation_dict['series'] = time_series
                                        occupations_timeseries[occupation] = occupation_dict
                            if enriched.get('skill'):
                                skills = enriched['skill']
                                for skill in skills:
                                    if skill not in skills_timeseries:
                                        skills_timeseries[skill] = {"skill": skill, "series": [{"date_timemillis": publication_date_millis, "occurrences": number_of_vacancies}]}
                                        # skills_timeseries[skill] = {"skill": skill, "series": [{"date_timemillis": publication_date_millis, "occurences": number_of_vacancies}], "ads": [{"id": ad.get("id"), "original_id": ad.get("original_id")}]}
                                    else:
                                        skill_dict = skills_timeseries[skill]
                                        # add ad to ads.
                                        # ads = skill_dict['ads']
                                        # ads.append({"id": ad.get("id"), "original_id": ad.get("original_id")})
                                        # skill_dict['ads'] = ads
                                        time_series = skill_dict['series']
                                        # Check if on same date_timemillis, if so increment occurences.
                                        date_match = False
                                        for time_serie_dict in time_series:
                                            if time_serie_dict['date_timemillis'] == publication_date_millis:
                                                time_serie_dict["occurrences"] = time_serie_dict["occurrences"] + number_of_vacancies
                                                date_match = True
                                                break
                                        if not date_match:
                                            # else add as new element in series list.
                                            time_series.append({"date_timemillis": publication_date_millis, "occurrences": number_of_vacancies})
                                        skill_dict['series'] = time_series
                                        skills_timeseries[skill] = skill_dict

                    #log.debug('parsed ad' + ad.get('id'))
                log.info('ready parsing')

                log.info('Preparing to store skills...')
                skills_timeseries_to_upload = []

                for skill_term, skill_series in skills_timeseries.items():
                    skills_timeseries_to_upload.append(skill_series)

                log.info('Storing skills in minio bucket...')
                # Upload merged file to minio...
                if skills_timeseries_to_upload and len(skills_timeseries_to_upload) > 0:
                    file_name_prefix = f'skills_{year}'

                    minio_uploader.upload_array_to_ndjson_file(skills_timeseries_to_upload, file_name_prefix)

                log.info('Preparing to store occupations...')
                occupations_timeseries_to_upload = []
                for occupation_term, occupation_series in occupations_timeseries.items():
                    occupations_timeseries_to_upload.append(occupation_series)

                log.info('Storing occupations in minio bucket...')
                # Upload merged file to minio...
                if occupations_timeseries_to_upload and len(occupations_timeseries_to_upload) > 0:
                    file_name_prefix = f'occupations_{year}'

                    minio_uploader.upload_array_to_ndjson_file(occupations_timeseries_to_upload, file_name_prefix)

                f = open("ad_ids.txt", "w")
                if len(ads_found) > 0:
                    for key, value in ads_found.items():
                        f.write(key + "\n")
                f.close()
                log.info("ready")
